import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { PageComponent } from './containers/page/page.component';
import { DataFormRoutingModule } from './data-form-routing.module';

@NgModule({
  declarations: [PageComponent],
  imports: [
    DataFormRoutingModule,
    SharedModule
  ]
})
export class DataFormModule { }
