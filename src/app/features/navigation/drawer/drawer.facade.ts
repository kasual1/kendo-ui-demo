import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CustomDrawerItem } from './models/custom-drawer-item.model';
import { DrawerState } from './state/drawer.state';

@Injectable({ providedIn: 'root' })
export class DrawerFacade {

    constructor(
        private drawerState: DrawerState
    ) { }

    isDrawerExpanded$(): Observable<boolean> {
        return this.drawerState.getExpanded$();
    }

    getDrawerItems$(): Observable<CustomDrawerItem[]> {
        return this.drawerState.getItems$()
    }

    setDrawerExpanded(expanded: boolean): void {
        this.drawerState.setExpanded(expanded);
    }

    toggleDrawerExpanded(): void {
        const expanded = this.drawerState.getExpanded();
        this.drawerState.setExpanded(!expanded);
    }

    expandDrawerItem(drawerItem: CustomDrawerItem): void {
        const text = drawerItem.text;
        if (!drawerItem.parent) {
            return;
        }

        const newItems = this.resetItems();
        const index = newItems.findIndex((i) => i.text === text);
        newItems[index].selected = true;

        if (!drawerItem.expanded) {
            newItems[index].expanded = true;
            this.addChildren(newItems, index, newItems[index].children);
        }
        else {
            newItems[index].expanded = false;
            this.removeChildren(newItems, index, newItems[index].children);
        }
        this.drawerState.setItems(newItems)
    }

    private addChildren(arr: any, index: number, children: Array<any>) {
        arr.splice(index + 1, 0, ...children);
    }

    private removeChildren(arr: any, index: number, children: Array<any>) {
        arr.splice(index + 1, children.length);
    }

    private resetItems(): Array<any> {
        const arr: any = [];
        this.drawerState.getItems().forEach((item: any) => {
            arr.push(Object.assign({}, item));
        });
        return arr;
    }

}