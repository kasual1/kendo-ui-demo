import { NgModule } from '@angular/core';
import { DrawerComponent } from './containers/drawer/drawer.component';
import { FooterComponent } from './components/footer/footer.component';
import { ItemComponent } from './components/item/item.component';
import { LayoutModule } from '@progress/kendo-angular-layout';
import { SharedModule } from '../../../shared/shared.module';
import { BreadcrumbsModule } from '../breadcrumbs/breadcrumbs.module';
import { ButtonsModule } from '@progress/kendo-angular-buttons';

@NgModule({
  declarations: [
    DrawerComponent,
    FooterComponent,
    ItemComponent
  ],
  imports: [
    SharedModule,
    LayoutModule,
    BreadcrumbsModule,
  ],
  exports: [
    DrawerComponent
  ]
})
export class DrawerModule { }
