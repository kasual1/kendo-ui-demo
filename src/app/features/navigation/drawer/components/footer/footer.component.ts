import { ChangeDetectionStrategy, Component, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FooterComponent implements OnInit {

  @Input()
  expanded: boolean;

  constructor() { }

  ngOnInit(): void {
  }

}
