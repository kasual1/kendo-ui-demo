import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { CustomDrawerItem } from '../../models/custom-drawer-item.model';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ItemComponent implements OnInit {

  @Input()
  expanded: boolean;

  @Input()
  item: CustomDrawerItem;

  constructor() { }

  ngOnInit(): void {
  }

}
