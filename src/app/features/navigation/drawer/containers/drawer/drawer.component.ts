import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DrawerSelectEvent } from '@progress/kendo-angular-layout';
import { Observable } from 'rxjs';
import { DrawerFacade } from '../../drawer.facade';
import { CustomDrawerItem } from '../../models/custom-drawer-item.model';
@Component({
  selector: 'app-drawer',
  templateUrl: './drawer.component.html',
  styleUrls: ['./drawer.component.scss']
})
export class DrawerComponent implements OnInit {

  expanded$: Observable<boolean>;

  items$: Observable<CustomDrawerItem[]>;

  constructor(
    private router: Router,
    private drawerFacade: DrawerFacade
  ) {
    this.expanded$ = this.drawerFacade.isDrawerExpanded$();
    this.items$ = this.drawerFacade.getDrawerItems$();
  }

  ngOnInit(): void {
  }

  handleSelect(event: DrawerSelectEvent): void {
    const item = event.item;
    if (!item.children) {
      this.router.navigate([item.path])
    } else {
      this.drawerFacade.expandDrawerItem(item);
    }
  }

  handleClick(): void {
    this.drawerFacade.toggleDrawerExpanded();
  }

}
