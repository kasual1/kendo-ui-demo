import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { CustomDrawerItem } from '../models/custom-drawer-item.model';

@Injectable({ providedIn: 'root' })
export class DrawerState {

    private readonly expanded$ = new BehaviorSubject<boolean>(true);

    private readonly items$ = new BehaviorSubject<CustomDrawerItem[]>([
        {
            text: 'Home',
            path: '/home',
            icon: 'k-i-home',
            parent: true,
        },
          {
            text: 'Styling',
            path: '/styling',
            icon: 'k-i-pencil',
            parent: true
        },
        {
            text: 'Data Form',
            path: '/data-form',
            icon: 'k-i-file-txt',
            parent: true,
        },
        {
            text: 'Data Table',
            path: '/data-table',
            icon: 'k-i-grid',
            expanded: false,
            parent: true,
            children: [
                {
                    text: 'People',
                    path: '/data-table/people',
                    icon: 'k-i-grid',
                    parent: false,
                },
                {
                    text: 'Starships',
                    path: '/data-table/starship',
                    icon: 'k-i-grid',
                    parent: false,
                },
                {
                    text: 'Posts',
                    path: '/data-table/post',
                    icon: 'k-i-grid',
                    parent: false,
                }
            ]
        }
    ]);

    constructor() { }

    getExpanded$(): Observable<boolean> {
        return this.expanded$.asObservable();
    }

    getItems$(): Observable<CustomDrawerItem[]> {
        return this.items$.asObservable();
    }

    getExpanded(): boolean {
        return this.expanded$.getValue();
    }

    getItems(): CustomDrawerItem[] {
        return this.items$.getValue();
    }

    setExpanded(expanded: boolean): void {
        this.expanded$.next(expanded);
    }

    setItems(items: any[]): void {
        this.items$.next(items);
    }


}
