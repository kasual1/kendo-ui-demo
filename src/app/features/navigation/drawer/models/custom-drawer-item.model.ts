import { DrawerItem } from "@progress/kendo-angular-layout";

export interface CustomDrawerItem extends DrawerItem{
    title?: string;
    path?: string;
    parent?: boolean;
    expanded?: boolean;
    children?: CustomDrawerItem[];
}