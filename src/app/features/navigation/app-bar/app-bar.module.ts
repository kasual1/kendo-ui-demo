import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IconModule } from '@progress/kendo-angular-icons';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { AvatarModule } from '@progress/kendo-angular-layout';
import { NavigationModule } from '@progress/kendo-angular-navigation';
import { AppBarComponent } from './components/app-bar/app-bar.component';

@NgModule({
  declarations: [AppBarComponent],
  imports: [
    AvatarModule,
    CommonModule,
    IconModule,
    NavigationModule,
    InputsModule
  ],
  exports: [
    AppBarComponent
  ]
})
export class AppBarModule { }
