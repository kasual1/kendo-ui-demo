import { Component, OnInit } from '@angular/core';
import { ChangeEvent } from '@progress/kendo-angular-dialog/dist/es2015/window/window-events';

@Component({
  selector: 'app-app-bar',
  templateUrl: './app-bar.component.html',
  styleUrls: ['./app-bar.component.scss']
})
export class AppBarComponent implements OnInit {

  public kendokaAvatar = 'https://i.stack.imgur.com/l60Hf.png';

  powerUserMode: boolean = false;

  constructor() { }

  ngOnInit(): void {
    this.handleDarkModeChange(false);
  }

  handlePowerUserModeChange(): void {
    this.powerUserMode = !this.powerUserMode;
    if(this.powerUserMode){
      document.body.classList.add('small');
      document.body.classList.remove('large');
    } else {
      document.body.classList.add('large');
      document.body.classList.remove('small');
    }
  }


  handleDarkModeChange(darkMode: boolean): void {
   if(darkMode){
     document.body.classList.add('theme-dark');
     document.body.classList.remove('theme-light');
   } else {
     document.body.classList.add('theme-light');
     document.body.classList.remove('theme-dark');
   }
  }

}
