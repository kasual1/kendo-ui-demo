import { NgModule } from '@angular/core';
import { BreadcrumbComponent } from './containers/breadcrumb/breadcrumb.component';
import { NavigationModule } from '@progress/kendo-angular-navigation';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    BreadcrumbComponent
  ],
  imports: [
    CommonModule,
    NavigationModule
  ],
  exports: [
    BreadcrumbComponent
  ]
})
export class BreadcrumbsModule { }
