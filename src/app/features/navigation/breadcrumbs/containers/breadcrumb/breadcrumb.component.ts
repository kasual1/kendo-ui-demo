import { Component, OnInit } from '@angular/core';
import { BreadCrumbItem } from '@progress/kendo-angular-navigation';
import { Observable } from 'rxjs';
import { BreadcrumbFacade } from '../../breadcrumb.facade';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent implements OnInit {

  public items$: Observable<BreadCrumbItem[]>;

  constructor(private breadcrumbFacade: BreadcrumbFacade) {
    this.items$ = this.breadcrumbFacade.getBreadcrumbItems$();
  }

  ngOnInit(): void {

  }

}
