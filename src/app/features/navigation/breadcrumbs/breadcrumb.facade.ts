import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot } from '@angular/router';
import { BreadCrumbItem } from '@progress/kendo-angular-navigation';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { AppState } from 'src/app/app.state';
import { getUniqueListBy } from 'src/app/core/helpers/array.helper';

@Injectable({ providedIn: 'root' })
export class BreadcrumbFacade {

    constructor(private appState: AppState) { }

    getBreadcrumbItems$(): Observable<any[]> {
        return this.appState.getRouterSnapshot$()
            .pipe(
                filter(data => data !== null),
                map(data => {
                    const traversed = this.traverse(data as ActivatedRouteSnapshot, []);
                    const breadcrumbs = traversed
                        .map(snapshot => {
                            return {
                                text: snapshot?.data?.breadcrumb,
                                title: snapshot?.data?.breadcrumb
                            } as BreadCrumbItem
                        });
                    const uniqueBreadcrumbs = getUniqueListBy(breadcrumbs, 'text');
                    return uniqueBreadcrumbs;
                })
            );
    }

    private traverse(snapshot: ActivatedRouteSnapshot, flattenedSnapshots: ActivatedRouteSnapshot[]) {
        if (snapshot.firstChild) {
            flattenedSnapshots.push(snapshot.firstChild);
            this.traverse(snapshot.firstChild, flattenedSnapshots);
        }
        return flattenedSnapshots;
    }

}