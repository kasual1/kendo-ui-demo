import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StylingRoutingModule } from './styling-routing.module';
import { StylingComponent } from './containers/styling/styling.component';
import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { CardComponent } from './components/card/card.component';
import { SubCardComponent } from './components/sub-card/sub-card.component';
import { GridModule } from '@progress/kendo-angular-grid';


@NgModule({
  declarations: [StylingComponent, CardComponent, SubCardComponent],
  imports: [
    CommonModule,
    StylingRoutingModule,
    ButtonsModule,
    GridModule
  ]
})
export class StylingModule { }
