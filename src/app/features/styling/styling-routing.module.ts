import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StylingComponent } from './containers/styling/styling.component';

const routes: Routes = [
  {
    path: '',
    component: StylingComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StylingRoutingModule { }
