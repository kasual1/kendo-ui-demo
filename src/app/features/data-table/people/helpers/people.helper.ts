export function getIdFromUrl(peopleUrl: string): number {
    const segments = peopleUrl.split('/');
    const id = Number.parseInt(segments[5]);
    return id;
}