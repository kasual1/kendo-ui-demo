import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PeopleGridComponent } from './containers/people-grid/people-grid.component';
import { GridModule } from '@progress/kendo-angular-grid';
import { PeopleDetailComponent } from './containers/people-detail/people-detail.component';
import { RouterModule } from '@angular/router';
import { PeopleRoutingModule } from './people-routing.module';

@NgModule({
  declarations: [
    PeopleGridComponent,
    PeopleDetailComponent,
    
  ],
  imports: [
    CommonModule,
    RouterModule,
    GridModule,
    PeopleRoutingModule
  ],
  exports: [
    PeopleGridComponent,
    PeopleDetailComponent
  ]
})
export class PeopleModule { }
