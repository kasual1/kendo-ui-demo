import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RefreshStrategy } from 'src/app/core/enums/caching.enums';
import { Page } from 'src/app/core/models/page.model';
import { environment } from 'src/environments/environment';
import { People } from '../models/people.model';

@Injectable({ providedIn: 'root' })
export class PeopleService {

    private url: string = `${environment.baseApiUrlSwapi}/people`

    constructor(private http: HttpClient) { }

    getByPage(page: number, refreshStrategy?: RefreshStrategy): Observable<Page<People[]>> {
        const headers = new HttpHeaders().set('x-refresh', refreshStrategy ?? '');
        return this.http.get<Page<People[]>>(`${this.url}/?page=${page}`, { headers });
    }

}