import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PeopleDetailComponent } from './containers/people-detail/people-detail.component';
import { PeopleGridComponent } from './containers/people-grid/people-grid.component';

const routes: Routes = [
  {
    path: '',
    component: PeopleGridComponent
  },
  {
    path: ':id',
    data: { breadcrumb: 'Detail' },
    component: PeopleDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PeopleRoutingModule { }