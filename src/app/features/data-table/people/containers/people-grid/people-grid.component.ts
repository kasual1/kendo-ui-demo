import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { People } from '../../models/people.model';
import { PeopleFacade } from '../../people.facade';

@Component({
  selector: 'app-people-grid',
  templateUrl: './people-grid.component.html',
  styleUrls: ['./people-grid.component.scss']
})
export class PeopleGridComponent implements OnInit {

  pageSize$: Observable<number>;

  skip$: Observable<number>;

  items$: Observable<{ data: People[], total: number }>;

  loading$: Observable<boolean>;

  constructor(private peopleFacade: PeopleFacade) {
    this.pageSize$ = this.peopleFacade.getPageSize$();
    this.items$ = this.peopleFacade.getItems$();
    this.loading$ = this.peopleFacade.isLoading$();
    this.skip$ = this.peopleFacade.getSkip$();
  }

  ngOnInit(): void {
    this.peopleFacade.loadItems$();
  }

  handleChange(event: any): void {
    this.peopleFacade.setSkip(event.skip);
    this.peopleFacade.setTake(event.take);
    this.peopleFacade.loadItems$();
  }

  handleCellClick(event: any): void {
    this.peopleFacade.navigateToDetail(event.rowIndex);
  }

}
