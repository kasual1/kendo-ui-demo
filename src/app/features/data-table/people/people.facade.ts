import { Injectable } from '@angular/core';
import { combineLatest, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { PeopleState } from './state/people.state';
import { PeopleService } from './services/people.service';
import { Router } from '@angular/router';
import { getIdFromUrl } from './helpers/people.helper';
import { RefreshStrategy } from 'src/app/core/enums/caching.enums';

@Injectable({ providedIn: 'root' })
export class PeopleFacade {

    constructor(
        private peopleService: PeopleService,
        private peopleState: PeopleState,
        private router: Router
    ) { }

    getPageSize$(): Observable<number> {
        return this.peopleState.getPageSize$();
    }

    getSkip$(): Observable<number> {
        return this.peopleState.getSkip$();
    }

    getTake$(): Observable<number> {
        return this.peopleState.getTake$();
    }

    isLoading$(): Observable<boolean> {
        return this.peopleState.getLoading$();
    }

    getItems$(): Observable<{ data: any[], total: number }> {
        return combineLatest([
            this.peopleState.getItems$(),
            this.peopleState.getTotal$()
        ]).pipe(
            map(([items, total]) => {
                return {
                    data: items,
                    total
                }
            })
        );
    }

    loadItems$(): void {
        const page = this.peopleState.getSkip() / this.peopleState.getTake() + 1;
        this.peopleState.setLoading(true);
        this.peopleService.getByPage(page, RefreshStrategy.STALE_WHILE_REVALIDATE)
            .subscribe(
                page => {
                    this.peopleState.setItems(page.results);
                    this.peopleState.setTotal(page.count);
                    this.peopleState.setLoading(false);
                },
                err => console.error(err)
            );
    }

    setSkip(skip: number): void {
        this.peopleState.setSkip(skip);
    }

    setTake(take: number): void {
        this.peopleState.setTake(take);
    }

    navigateToDetail(rowIndex: number): void {
        const pageSize = this.peopleState.getPageSize();
        const relativeRowIndex = rowIndex % pageSize;
        const items = this.peopleState.getItems();
        const item = items[relativeRowIndex];
        const id = getIdFromUrl(item.url);
        this.router.navigate(['/data-table/people', id]);
    }

}