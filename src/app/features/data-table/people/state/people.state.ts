import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { People } from '../models/people.model';

@Injectable({providedIn: 'root'})
export class PeopleState {
    
    private readonly items = new BehaviorSubject<People[]>([]);

    private readonly total = new BehaviorSubject<number>(0);

    private readonly pageSize = new BehaviorSubject<number>(10);

    private readonly skip = new BehaviorSubject<number>(0);

    private readonly take = new BehaviorSubject<number>(10);

    private readonly loading = new BehaviorSubject<boolean>(false);
    
    getItems$(): Observable<People[]>{
        return this.items.asObservable();
    }

    getTotal$(): Observable<number> {
        return this.total.asObservable();
    }

    getPageSize$(): Observable<number> {
        return this.pageSize.asObservable();
    }

    getSkip$(): Observable<number> {
        return this.skip.asObservable();
    }

    getTake$(): Observable<number> {
        return this.take.asObservable();
    }

    getLoading$(): Observable<boolean>{
        return this.loading.asObservable();
    }

    getPageSize(): number {
        return this.pageSize.getValue();
    }

    getItems(): People[]{
        return this.items.getValue();
    }

    getSkip(): number {
        return this.skip.getValue();
    }

    getTake(): number {
        return this.take.getValue();
    }

    setItems(items: People[]): void {
        this.items.next(items);
    }

    setTotal(total: number): void {
        this.total.next(total);
    }

    setSkip(skip: number): void {
        this.skip.next(skip);
    }

    setTake(take: number): void {
        this.take.next(take);
    }

    setLoading(loading: boolean): void {
        this.loading.next(loading);
    }
}