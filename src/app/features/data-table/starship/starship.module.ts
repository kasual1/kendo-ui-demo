import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StarshipDetailComponent } from './containers/starship-detail/starship-detail.component';
import { StarshipGridComponent } from './containers/starship-grid/starship-grid.component';
import { GridModule } from '@progress/kendo-angular-grid';
import { SharedModule } from '../../../shared/shared.module';
import { RouterModule } from '@angular/router';
import { StarshipRoutingModule } from './starship-routing.module';

@NgModule({
  declarations: [
    StarshipDetailComponent,
    StarshipGridComponent
  ],
  imports: [
    CommonModule,
    GridModule,
    RouterModule,
    SharedModule,
    StarshipRoutingModule
  ],
  exports: [
    StarshipDetailComponent,
    StarshipGridComponent
  ]
})
export class StarshipModule { }
