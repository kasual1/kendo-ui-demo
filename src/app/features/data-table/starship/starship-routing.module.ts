import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StarshipDetailComponent } from './containers/starship-detail/starship-detail.component';
import { StarshipGridComponent } from './containers/starship-grid/starship-grid.component';

const routes: Routes = [
  {
    path: '',
    component: StarshipGridComponent
  },
  {
    path: ':id',
    data: { breadcrumb: 'Detail' },
    component: StarshipDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StarshipRoutingModule { }