import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Starship } from '../../models/starship.model';
import { StarshipFacade } from '../../starship.facade';

@Component({
  selector: 'app-starship-grid',
  templateUrl: './starship-grid.component.html',
  styleUrls: ['./starship-grid.component.scss']
})
export class StarshipGridComponent implements OnInit {

  pageSize$: Observable<number>;

  skip$: Observable<number>;

  items$: Observable<{ data: Starship[], total: number }>;

  loading$: Observable<boolean>;

  constructor(private starshipFacade: StarshipFacade) {
    this.pageSize$ = this.starshipFacade.getPageSize$();
    this.items$ = this.starshipFacade.getItems$();
    this.loading$ = this.starshipFacade.isLoading$();
    this.skip$ = this.starshipFacade.getSkip$();
  }

  ngOnInit(): void {
    this.starshipFacade.loadItems$();
  }

  handleChange(event: any): void {
    this.starshipFacade.setSkip(event.skip);
    this.starshipFacade.setTake(event.take);
    this.starshipFacade.loadItems$();
  }

}
