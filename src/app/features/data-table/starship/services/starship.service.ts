import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RefreshStrategy } from 'src/app/core/enums/caching.enums';
import { Page } from 'src/app/core/models/page.model';
import { environment } from 'src/environments/environment';
import { Starship } from '../models/starship.model';

@Injectable({ providedIn: 'root' })
export class StarshipService {

    private url: string = `${environment.baseApiUrlSwapi}/starships`

    constructor(private http: HttpClient) { }

    getByPage(page: number, refreshStrategy?: RefreshStrategy): Observable<Page<Starship[]>> {
        const headers = new HttpHeaders().set('x-refresh', refreshStrategy ?? '');
        return this.http.get<Page<Starship[]>>(`${this.url}/?page=${page}`, { headers });
    }

}