import { Injectable } from '@angular/core';
import { Observable, combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';
import { RefreshStrategy } from 'src/app/core/enums/caching.enums';
import { StarshipService } from './services/starship.service';
import { StarshipState } from './state/starship.state';

@Injectable({providedIn: 'root'})
export class StarshipFacade {

    constructor(
        private starshipService: StarshipService,
        private starshipState: StarshipState
    ) { }

    getPageSize$(): Observable<number> {
        return this.starshipState.getPageSize$();
    }

    getSkip$(): Observable<number> {
        return this.starshipState.getSkip$();
    }

    getTake$(): Observable<number> {
        return this.starshipState.getTake$();
    }

    isLoading$(): Observable<boolean> {
        return this.starshipState.getLoading$();
    }

    getItems$(): Observable<{ data: any[], total: number }> {
        return combineLatest([
            this.starshipState.getItems$(),
            this.starshipState.getTotal$()
        ]).pipe(
            map(([items, total]) => {
                return {
                    data: items,
                    total
                }
            })
        );
    }

    loadItems$(): void {
        const page = Math.floor(this.starshipState.getSkip() / this.starshipState.getTake() + 1);
        this.starshipState.setLoading(true);
        this.starshipService.getByPage(page, RefreshStrategy.STALE_WHILE_REVALIDATE)
            .subscribe(
                page => {
                    this.starshipState.setItems(page.results);
                    this.starshipState.setTotal(page.count);
                    this.starshipState.setLoading(false);
                },
                err => console.error(err)
            );
    }

    setSkip(skip: number): void {
        this.starshipState.setSkip(skip);
    }

    setTake(take: number): void {
        this.starshipState.setTake(take);
    }
    
}