import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { RefreshStrategy } from 'src/app/core/enums/caching.enums';
import { Observable } from 'rxjs';
import { PostTableHeader } from '../models/post-table-header.model';

@Injectable({ providedIn: 'root' })
export class TableHeaderService {

    private url: string = `${environment.baseApiUrlJsonServer}/tableHeaders`

    constructor(private http: HttpClient) { }

    getAll(refreshStrategy?: RefreshStrategy): Observable<PostTableHeader[]> {
        const headers = new HttpHeaders().set('x-refresh', refreshStrategy ?? '');
        return this.http.get<PostTableHeader[]>(this.url, { headers });
    }

    getById(id: number, refreshStrategy?: RefreshStrategy): Observable<PostTableHeader> {
        const headers = new HttpHeaders().set('x-refresh', refreshStrategy ?? '');
        return this.http.get<PostTableHeader>(`${this.url}/${id}`, { headers });
    }

    patchById(id: number, tableHeader: PostTableHeader): Observable<PostTableHeader> {
        return this.http.patch<PostTableHeader>(`${this.url}/${id}`, tableHeader);
    }

}