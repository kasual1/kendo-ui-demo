import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RefreshStrategy } from 'src/app/core/enums/caching.enums';
import { environment } from 'src/environments/environment';
import { Post } from '../models/post.model';

@Injectable({ providedIn: 'root' })
export class PostService {

    private url: string = `${environment.baseApiUrlJsonServer}/posts`

    constructor(private http: HttpClient) { }

    getAll(refreshStrategy?: RefreshStrategy): Observable<Post[]> {
        const headers = new HttpHeaders().set('x-refresh', refreshStrategy ?? '');
        return this.http.get<Post[]>(this.url, { headers });
    }

    getAllByQuery(query: string, refreshStrategy?: RefreshStrategy): Observable<Post[]> {
        const headers = new HttpHeaders().set('x-refresh', refreshStrategy ?? '');
        const params = new HttpParams().set('q', query);
        return this.http.get<Post[]>(this.url, { headers, params });
    }

    create(post: Post): Observable<Post> {
        return this.http.post<Post>(this.url, post);
    }


}