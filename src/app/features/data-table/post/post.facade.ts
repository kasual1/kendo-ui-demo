import { Injectable } from '@angular/core';
import { Observable, forkJoin } from 'rxjs';
import { switchMap, filter, map } from 'rxjs/operators';
import { RefreshStrategy } from 'src/app/core/enums/caching.enums';
import { Post } from './models/post.model';
import { PostService } from './services/post.service';
import { PostState } from './state/post.state';
import { TableHeaderService } from './services/table-header.service';
import { TableHeaderItem } from './models/table-header-item.model';

@Injectable({ providedIn: 'root' })
export class PostFacade {

    constructor(
        private postService: PostService,
        private tableHeaderService: TableHeaderService,
        private postState: PostState
    ) { }

    getItems$(): Observable<Post[]> {
        return this.postState.getItems$();
    }

    getTableHeader$(): Observable<TableHeaderItem[]> {
        return this.postState.getTableHeader$()
            .pipe(
                filter(tableHeader => tableHeader !== null),
                map(tableHeader => tableHeader?.items as TableHeaderItem[])
            );
    }

    getLoading$(): Observable<boolean> {
        return this.postState.getLoading$();
    }

    loadItems(): void {
        this.postState.setLoading(true);

        forkJoin([
            this.postService.getAll(RefreshStrategy.STALE_WHILE_REVALIDATE),
            this.tableHeaderService.getById(1)
        ])
            .subscribe(
                ([posts, tableHeader]) => {
                    this.postState.setItems(posts);
                    this.postState.setTableHeader(tableHeader);
                    this.postState.setLoading(false);
                },
                err => {
                    console.error(err);
                    this.postState.setLoading(false);
                },

            );
    }

    refreshItems(): void {
        this.postState.setLoading(true);
        this.postService.getAll()
            .subscribe(
                posts => {
                    this.postState.setItems(posts);
                    this.postState.setLoading(false);
                },
                err => {
                    console.error(err);
                    this.postState.setLoading(false);
                }
            );
    }

    searchItems(query: string): void {
        this.postState.setLoading(true);
        this.postService.getAllByQuery(query, RefreshStrategy.STALE_WHILE_REVALIDATE)
            .subscribe(
                posts => {
                    this.postState.setItems(posts);
                    this.postState.setLoading(false);
                },
                err => {
                    console.error(err);
                    this.postState.setLoading(false);
                }
            );
    }

    createItem(post: Post): void {
        this.postState.setLoading(true);
        this.postService.create(post)
            .pipe(switchMap(() => this.postService.getAll()))
            .subscribe(
                posts => {
                    this.postState.setItems(posts);
                    this.postState.setLoading(false);
                },
                err => {
                    console.error(err);
                    this.postState.setLoading(false);
                }
            );
    }

    updateTableHeader(items: TableHeaderItem[]): void {
        const id = this.postState.getTableHeader()?.id;
        if (id) {
            this.tableHeaderService.patchById(id, { id, items })
                .subscribe(
                    tableHeader => this.postState.setTableHeader(tableHeader),
                    err => console.error(err)
                );
        }

    }
}