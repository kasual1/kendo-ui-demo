import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Post } from '../models/post.model';
import { PostTableHeader } from '../models/post-table-header.model';

@Injectable({ providedIn: 'root' })
export class PostState {

    private readonly items$ = new BehaviorSubject<Post[]>([]);

    private readonly tableHeader$ = new BehaviorSubject<PostTableHeader | null>(null);

    private readonly loading$ = new BehaviorSubject<boolean>(false);

    getItems$(): Observable<Post[]> {
        return this.items$.asObservable();
    }

    getTableHeader$(): Observable<PostTableHeader | null> {
        return this.tableHeader$.asObservable();
    }

    getLoading$(): Observable<boolean> {
        return this.loading$.asObservable();
    }

    getTableHeader(): PostTableHeader | null {
        return this.tableHeader$.getValue();
    }

    setItems(items: Post[]): void {
        this.items$.next(items);
    }

    setTableHeader(tableHeader: PostTableHeader): void {
        this.tableHeader$.next(tableHeader);
    }

    setLoading(loading: boolean): void {
        this.loading$.next(loading);
    }
}