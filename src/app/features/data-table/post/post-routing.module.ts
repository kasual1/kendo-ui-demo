import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PostGridComponent } from './containers/post-grid/post-grid.component';

const routes: Routes = [
  {
    path: '',
    component: PostGridComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PostRoutingModule { }
