export interface Post {
    id?: number
    title?: string,
    author?: string,
    published?: boolean
}