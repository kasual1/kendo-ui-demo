import { TableHeaderItem } from './table-header-item.model';

export interface PostTableHeader {
    id: number;
    items: TableHeaderItem[];
}