import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PostRoutingModule } from './post-routing.module';
import { PostGridComponent } from './containers/post-grid/post-grid.component';
import { GridModule } from '@progress/kendo-angular-grid';
import { PostToolbarComponent } from './components/post-toolbar/post-toolbar.component';
import { ToolBarModule } from '@progress/kendo-angular-toolbar';
import { ButtonModule } from '@progress/kendo-angular-buttons';
import { DialogModule } from '@progress/kendo-angular-dialog';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { FormsModule } from '@angular/forms';
import { LabelModule } from '@progress/kendo-angular-label';
import { PostDialogComponent } from './components/post-dialog/post-dialog.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [
    PostGridComponent,
    PostToolbarComponent,
    PostDialogComponent,
  ],
  imports: [
    CommonModule,
    PostRoutingModule,
    GridModule,
    ToolBarModule,
    ButtonModule,
    DialogModule,
    InputsModule,
    FormsModule,
    LabelModule,
    SharedModule
  ]
})
export class PostModule { }
