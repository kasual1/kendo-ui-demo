import { Component, OnInit } from '@angular/core';
import { DialogService } from '@progress/kendo-angular-dialog';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { PostDialogComponent } from '../../components/post-dialog/post-dialog.component';
import { Post } from '../../models/post.model';
import { PostFacade } from '../../post.facade';
import { TableHeaderItem } from '../../models/table-header-item.model';
import { switchItemPositions } from 'src/app/core/helpers/array.helper';

@Component({
  selector: 'app-post-grid',
  templateUrl: './post-grid.component.html',
  styleUrls: ['./post-grid.component.scss']
})
export class PostGridComponent implements OnInit {

  items$: Observable<Post[]>;

  columns$: Observable<TableHeaderItem[]>;

  loading$: Observable<boolean>;

  columns: TableHeaderItem[];

  query: string;

  constructor(
    private postFacade: PostFacade,
    private dialogService: DialogService
  ) {
    this.items$ = this.postFacade.getItems$();
    this.columns$ = this.postFacade.getTableHeader$();
    this.loading$ = this.postFacade.getLoading$();
  }

  ngOnInit(): void {
    this.postFacade.loadItems();

    this.postFacade.getTableHeader$()
      .subscribe(tableHeader => this.columns = tableHeader)
  }

  handleRefresh(): void {
    this.postFacade.refreshItems();
  }

  handleCreate(): void {
    const dialogRef = this.dialogService.open({
      title: 'Create New Post',
      content: PostDialogComponent
    });

    const postDialogComponent = dialogRef.content.instance as PostDialogComponent;
    postDialogComponent.post = {
      title: '',
      author: '',
      published: true
    };

    dialogRef.result
      .pipe(
        filter((result: any) => result?.post),
        map((result: any) => result.post)
      )
      .subscribe((post: Post) => this.postFacade.createItem(post));
  }

  handleSearch(query: string): void {
    this.query = query;
    this.postFacade.searchItems(query);
  }

  handleReorder(event: any): void {
    const reorderedColumns: TableHeaderItem[] = switchItemPositions(this.columns, event.oldIndex, event.newIndex);
    this.postFacade.updateTableHeader(reorderedColumns);
  }

}
