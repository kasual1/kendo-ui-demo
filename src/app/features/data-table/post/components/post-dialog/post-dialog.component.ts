import { Component, EventEmitter, Input, Output } from '@angular/core';
import { DialogContentBase, DialogRef } from '@progress/kendo-angular-dialog';
import { Post } from '../../models/post.model';

@Component({
  selector: 'app-post-dialog',
  templateUrl: './post-dialog.component.html',
  styleUrls: ['./post-dialog.component.scss']
})
export class PostDialogComponent extends DialogContentBase {

  @Input()
  post: Post;

  constructor(dialog: DialogRef) {
    super(dialog);
  }

  onSubmit(post: Post, valid: boolean | null): void {
    this.dialog.close({ post })
  }

}
