import { AfterViewInit, ChangeDetectionStrategy, Component, ElementRef, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { fromEvent } from 'rxjs';
import { map, debounceTime, distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'app-post-toolbar',
  templateUrl: './post-toolbar.component.html',
  styleUrls: ['./post-toolbar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PostToolbarComponent implements AfterViewInit {

  @Output()
  create = new EventEmitter();

  @Output()
  refresh = new EventEmitter();

  @Output()
  search = new EventEmitter<string>();

  @ViewChild('searchInput')
  searchInput: ElementRef<HTMLInputElement>;

  ngAfterViewInit(): void {
    fromEvent(this.searchInput.nativeElement, 'input')
      .pipe(
        map(event => (event.target as HTMLInputElement).value),
        map(query => query.length > 2 ? query : ''),
        debounceTime(50),
        distinctUntilChanged()
      )
      .subscribe((query) => this.search.emit(query));
  }

  onRefresh(): void {
    this.refresh.emit();
  }

  onCreate(): void {
    this.create.emit();
  }
}
