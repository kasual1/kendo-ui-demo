import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { RouterModule } from '@angular/router';
import { IconsModule } from '@progress/kendo-angular-icons';
import { HttpClientModule } from '@angular/common/http';
import { GridModule } from '@progress/kendo-angular-grid';
import { RatingComponent } from './components/rating/rating.component';
import { RippleModule } from '@progress/kendo-angular-ripple';
import { HighlightPipe } from './pipes/highlight.pipe';

@NgModule({
  declarations: [RatingComponent, HighlightPipe],
  imports: [
    CommonModule,
    ButtonsModule,
    RouterModule,
    IconsModule,
    HttpClientModule,
    GridModule,
    RippleModule,
  ],
  exports: [
    CommonModule,
    ButtonsModule,
    RouterModule,
    IconsModule,
    HttpClientModule,
    GridModule,
    RippleModule,

    RatingComponent,

    HighlightPipe
  ]
})
export class SharedModule { }
