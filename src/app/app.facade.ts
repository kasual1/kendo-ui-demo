import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { AppState } from './app.state';

@Injectable({ providedIn: 'root' })
export class AppFacade {

    constructor(private appState: AppState) { }
    
    getUrl$(): Observable<string> {
        return this.appState.getUrl$();
    }
    
    setUrl(url: string): void {
        return this.appState.setUrl(url);
    }
    
    setRouterSnapshot(routerSnapshot: ActivatedRouteSnapshot): void {
        this.appState.setRouterSnapshot(routerSnapshot);
    }
}