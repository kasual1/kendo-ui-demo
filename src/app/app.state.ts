import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class AppState {

    private readonly url = new BehaviorSubject<string>('');

    private readonly routerSnapshot = new BehaviorSubject<ActivatedRouteSnapshot | null>(null);

    constructor() { }

    getRouterSnapshot$(): Observable<ActivatedRouteSnapshot | null> {
        return this.routerSnapshot.asObservable();
    }

    getRouterSnapshot(): ActivatedRouteSnapshot | null {
        return this.routerSnapshot.getValue();
    }

    setRouterSnapshot(routeSnapshot: ActivatedRouteSnapshot): void {
        this.routerSnapshot.next(routeSnapshot);
    }

    getUrl$(): Observable<string> {
        return this.url.asObservable();
    }

    getUrl(): string {
        return this.url.getValue();
    }

    setUrl(url: string): void {
        this.url.next(url);
    }

}