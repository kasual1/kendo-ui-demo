import { HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class CachingService {

    constructor() { }

    get(request: HttpRequest<unknown>): Object {
        const storageItem = localStorage.getItem(request.urlWithParams)!;
        return JSON.parse(storageItem);
    }

    set(request: HttpRequest<unknown>, response: HttpResponse<unknown>): void {
        localStorage.setItem(request.urlWithParams, JSON.stringify(response));
    }

}