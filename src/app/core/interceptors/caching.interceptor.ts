import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
  HttpClient,
  HttpHeaders,
} from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { isCachable } from '../helpers/caching.helper';
import { CachingService } from '../services/caching.service';
import { skipLast, startWith, tap } from 'rxjs/operators';
import { RefreshStrategy } from '../enums/caching.enums';

@Injectable()
export class CachingInterceptor implements HttpInterceptor {

  constructor(private cache: CachingService, private http: HttpClient) { }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    if (!isCachable(request)) {
      return next.handle(request);
    }

    const cachedResponse = this.cache.get(request);

     // cache or fetch
     if (request.headers.get('x-refresh') === RefreshStrategy.CACHE_OR_FETCH) {
      return cachedResponse ? of(new HttpResponse<unknown>(cachedResponse)) : this.sendRequest(request, next);
    }

    // cache then fetch
    if (request.headers.get('x-refresh') === RefreshStrategy.CACHE_THEN_FETCH) {
      const results$ = this.sendRequest(request, next);
      return cachedResponse ? results$.pipe(startWith(new HttpResponse<unknown>(cachedResponse))) : results$;
    }

    // stale while revalidate
    if (request.headers.get('x-refresh') === RefreshStrategy.STALE_WHILE_REVALIDATE) {
      const results$ = this.sendRequest(request, next);
      return cachedResponse ? results$.pipe(startWith(new HttpResponse<unknown>(cachedResponse)), skipLast(1)) : results$;
    }

    // refresh
    return this.sendRequest(request, next);
  }

  private sendRequest(request: HttpRequest<unknown>, next: HttpHandler) {
    const noCustomHeaderRequest = request.clone({ headers: new HttpHeaders() });
    return next.handle(noCustomHeaderRequest)
      .pipe(
        tap(event => {
          if (event instanceof HttpResponse) {
            this.cache.set(request, event)
          }
        }),
      );
  }


}
