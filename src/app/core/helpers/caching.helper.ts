import { HttpRequest } from "@angular/common/http";

export function isCachable(request: HttpRequest<unknown>): boolean {
    return request.method === 'GET';
}

