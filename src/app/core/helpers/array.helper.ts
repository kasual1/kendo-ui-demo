export function getUniqueListBy(arr: any[], key: string) {
    return [...new Map(arr.map(item => [item[key], item])).values()]
}

export function switchItemPositions(arr: any[], fromIndex: number, toIndex: number): any[] {
    const element = arr[fromIndex];
    arr.splice(fromIndex, 1);
    arr.splice(toIndex, 0, element);
    return arr;
}