export enum RefreshStrategy {
    CACHE_OR_FETCH = 'cache-or-fetch',
    CACHE_THEN_FETCH = 'cache-then-fetch',
    STALE_WHILE_REVALIDATE = 'state-while-revalidate',
}