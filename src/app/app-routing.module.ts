import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full',
  },
  {
    path: 'home',
    loadChildren: () => import('./features/home/home.module').then(m => m.HomeModule),
    data: { breadcrumb: 'Home' }
  },
  {
    path: 'styling',
    loadChildren: () => import('./features/styling/styling.module').then(m => m.StylingModule),
    data: { breadcrumb: 'Styling' }
  },
  {
    path: 'data-form',
    loadChildren: () => import('./features/data-form/data-form.module').then(m => m.DataFormModule),
    data: { breadcrumb: 'Data Form' }

  },
  {
    path: 'data-table',
    data: { breadcrumb: 'Data Table' },
    children: [
      {
        path: 'people',
        data: { breadcrumb: 'People' },
        loadChildren: () => import('./features/data-table/people/people.module').then(m => m.PeopleModule)
      },
      {
        path: 'starship',
        data: { breadcrumb: 'Starship' },
        loadChildren: () => import('./features/data-table/starship/starship.module').then(m => m.StarshipModule)
      },
      {
        path: 'post',
        data: { breadcrumb: 'Post' },
        loadChildren: () => import('./features/data-table/post/post.module').then(m => m.PostModule)
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    paramsInheritanceStrategy: 'always',


  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
