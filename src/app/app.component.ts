import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router, RoutesRecognized } from '@angular/router';
import { filter, map, tap } from 'rxjs/operators';
import { AppFacade } from './app.facade';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  title = 'kendo-ui-demo';

  constructor(private router: Router, private appFacade: AppFacade) { }

  ngOnInit(): void {

    this.router.events
      .pipe(
        filter(event => event instanceof NavigationEnd),
        map(() => this.router.routerState.snapshot.root),
      )
      .subscribe(routeSnapshot => this.appFacade.setRouterSnapshot(routeSnapshot));

    this.router.events
      .pipe(
        filter(events => events instanceof RoutesRecognized),
        map(event => event as RoutesRecognized)
      )
      .subscribe(event => this.appFacade.setUrl(event.url));
  }

}
